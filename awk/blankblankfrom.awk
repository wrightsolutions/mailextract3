BEGIN {
	empty = 0;
	spacesonly = 0;
}

/From / {
	if (empty > 1) {
		print;
	} else {
		print " ";
		print " ";
		print;
	}
	empty = 0;
	spacesonly = 0;
	next;
}

/^ *$/ {
	spacesonly+=1;
	print;
	next;
}

/^$/ {
	empty+=1;
	next;
}

{
     print;
}
