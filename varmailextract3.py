#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2018 Gary Wright wrightsolutions.co.uk/contact
# 
# Redistribution and use in source and binary forms,
# with or without modification,
# are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation and/or
# other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor
# the names of its contributors may be used to endorse or promote
# products derived from this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
# FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

""" Script to extract mail items that have a content match with what supplied

First argument is a Python regex (or valid string)

cat /var/mail/someuser | ./varmailextract.py 'anacron'

Set PYVERBOSITY=2 or higher for more debug output
#export PYVERBOSITY=2

For some mail system output to /var/ there might not be the mandatory 2 line spacing between emails.
In this case you want to preprocess using sed and perhaps the anacron search above would become

cat /var/mail/someuser | sed 's/^From / \n \nFrom /' | ./varmailextract.py 'anacron'

Note: The single space at start of ' \n \n' bit of sed is important if you are to
pass the regex RE_BLANK_WITH_SPACE which is very specific.
"""

import fileinput
import re
from collections import namedtuple
#from os import path,sep
from sys import argv
#from string import printable
prog = argv[0].strip()
from os import getenv as osgetenv

""" Next we gather environment variable PYVERBOSITY
which is a global setting for verbosity.
Note: Functions themselves may implement local function signature
overrides or additions and would then generally use
a local variable named 'verbosity' in conjunction with/instead of
this global setting
"""
PYVERBOSITY_STRING = osgetenv('PYVERBOSITY')
PYVERBOSITY = 1
if PYVERBOSITY_STRING is None:
	pass
else:
	try:
		PYVERBOSITY = int(PYVERBOSITY_STRING)
	except:
		pass
#print(PYVERBOSITY)

SPACES2 = chr(32)*2


MAXLINES_LIST = 1000

RE_BLANK = re.compile('$')
RE_BLANK_WITH_SPACE = re.compile(r'{0}+$'.format(chr(32)))  # not ('\s+\Z')
RE_FROM = re.compile('From')
RE_RETURN_PATH = re.compile("Return-path:\s+<")
RE_RETURN_PATH_OLDER = re.compile("Return-Path:\s+<")
RE_ENVELOPE_TO = re.compile("Envelope-to:\s+")
#re_keepalive = re.compile('"\s[0-9]')



class LineBlock():

	def __init__(self, opening_linenum=0, opening_line=None, preopening_line='',
			closing_linenum=0, blanks_sequential=0, target_lines=[]):
		# Older nt LineBlock and initialisation was LineBlock(0,None,'',0,0,[])
		self._opening_linenum = opening_linenum
		self._opening_line = opening_line
		self._preopening_line = preopening_line
		self._closing_linenum = closing_linenum
		self._blanks_sequential = blanks_sequential
		self._target_lines = target_lines


	""" opening_linenum property requires no setter as mostly only set during __init__ """
	@property
	def opening_linenum(self):
		return self._opening_linenum

	""" opening_line property requires no setter as mostly only set during __init__ """
	@property
	def opening_line(self):
		return self._opening_line

	""" preopening_line property requires no setter as mostly only set during __init__ """
	@property
	def preopening_line(self):
		return self._preopening_line

	""" closing_linenum property requires no setter as mostly only set during __init__ """
	@property
	def closing_linenum(self):
		return self._closing_linenum

	def blanks_sequential_get(self):
		return self._blanks_sequential

	#def blanks_sequential_set(self,blanks_sequential):
	#	self._blanks_sequential = blanks_sequential

	def blanks_sequential_zero(self):
		self._blanks_sequential = 0

	def blanks_sequential_increment(self):
		self._blanks_sequential = 1+self.blanks_sequential_get()

	blanks_sequential = property(blanks_sequential_get)

	""" target_lines property requires no setter as mostly only set during __init__ """
	@property
	def target_lines(self):
		return self._target_lines

	def opener(self,linenum=None,opening=None,preopening=None,lines=[]):
		self._closing_linenum=0
		if linenum is None:
			return
		# Have a value supplied for linenum so set approprate field
		self._opening_linenum=linenum
		self._opening_line=opening
		self._preopening_line=preopening
		if len(lines) > 0:
			self._target_lines=lines
		if PYVERBOSITY > 2:
			prefix_str = 'Opener() has completed OPENING and opening_line now'
			print("{0} set to: {1}".format(prefix_str,self.opening_line))
		return


	def closer(self,linenum=None,lines=[]):
		""" Calling with just line number is usually a marker at start of
		a loop so fuller processing later in loop can take place.
		Supplying a list of lines will trigger full functionality
		"""
		if linenum is None:
			return self
		# Have a value supplied for linenum so set approprate field
		self._closing_linenum=linenum
		if len(lines) > 0:
			self._target_lines=lines
			#print("Closer() has completed and target_lines has len={0}".format(len(lines)))
		return


	def open(self):
		""" Boolean return for this 'query' style method """
		if self.opening_line is not None:
			return True
		return False


	def closed(self):
		""" Boolean return for this 'query' style method """
		if self.closing_linenum > 0:
			return True
		return False

		

total_linecount = 0
retain_flags = []
retain_lines = []


def dict_summary(dict_given,summary_level=1,print_flag=False,prefix_string=''):
	""" This method gives a summary [list] based on a given dictionary.
	Summary level controls the level and is normally set at 1 or 2
	1 sees any printed output just showing the LENGTH
	2 sees any printed output showing the unsorted keys
	3 sees any printed output showing the sorted keys
	Regardless of level, summary_as_list is ALWAYS unsorted list of keys
	"""
	summary_as_list = list(dict_given.keys())
	if print_flag is not True:
		return summary_as_list
	# Proceed based on summary_level
	dict_len = len(dict_given)
	dkeys = list(dict_given.keys())
	skeys = sorted(dict_given)
	if summary_level < 2:
		# We are going to limit our summary to showing just length
		if prefix_string is None or len(prefix_string) < 2:
			print("Dictionary contains {0} entries.".format(dict_len))
		else:
			print("{0} dictionary contains {1} entries.".format(prefix_string,
										dict_len))
	elif summary_level < 3:
		# Show keys (as found with no special effort to sort)
		if prefix_string is None or len(prefix_string) < 2:
			print('Dictionary contains the following key entries:')
		else:
			print("{0} dictionary contains the following keys:".format(prefix_string))
		print(dkeys)
	elif summary_level < 4:
		# Using skeys in here so SORTED output
		if prefix_string is None or len(prefix_string) < 2:
			print('Dictionary contains the following key entries:')
		else:
			print("{0} dictionary contains the following keys:".format(prefix_string))
		print(skeys)
		summary_as_list = skeys
	else:
		pass
	return summary_as_list


def dict_summary_dict(dict_given,summary_level=1,print_flag=False,prefix_string=''):
	""" Wraps dict_summary() and supports more verbose output of some fields like
	opening_linenum

	If summary_level is less than 3 then simply wrap entry and return summary_as_dict
	If print_flag is False then simply wrap entry and return summary_as_dict

	Primary reason for writing this wrapper is to facilitate calls from test
	harnesses and give more information about the matching results.
	"""
	summary_as_dict = {}
	if summary_level < 4 or print_flag is not True:
		summary_as_list = dict_summary(dict_given,summary_level,print_flag,prefix_string)
		for key in summary_as_list:
			summary_as_dict[key] = ''
	else:
		# We are going to take advantage of some of the extra printing functionaility
		pass

	if print_flag is not True:
		return summary_as_list
	# Proceed based on summary_level
	dict_len = len(dict_given)
	dkeys = list(dict_given.keys())
	skeys = sorted(dict_given)
	if summary_level < 5:
		# Show keys (as found with no special effort to sort)
		if prefix_string is None or len(prefix_string) < 2:
			print('Dictionary contains the following key entries:')
		else:
			print("{0} dictionary contains the following keys:".format(prefix_string))
		for k in dkeys:
			v = dict_given[k]
			line = "idx {0}: {1} {2} {3}".format(k,v[0],v[1],v[2]) 
			summary_as_dict[k] = line
			print(line)
	elif summary_level < 6:
		# Using skeys in here so SORTED output
		if prefix_string is None or len(prefix_string) < 2:
			print('Dictionary contains the following key entries:')
		else:
			print("{0} dictionary contains the following keys:".format(prefix_string))
		for k in skeys:
			v = dict_given[k]
			line = "idx {0}: {1} {2} {3}".format(k,v[0],v[1],v[2]) 
			summary_as_dict[k] = line
			print(line)
	else:
		pass

	return summary_as_dict


def list_from_target(opening_linenum,closing_linenum,opening_line,lines,maxlines=0,maxwidth=0):
	""" Primary reason for consulting list generated here is for test harness / debug
	So we give two parameters maxlines, maxwidth to control the brevity of what is stored
	and give a way for testing to operate on a subset of lines if desired.

	Supplying just the 4 arguments means there is no post processing / limiting and 
	the lines are stored in the list exactly as supplied.

	By contrast ...,100,50) is going to store a maximum of 100 lines AND
	each line stored is truncated to 50 wide.
	"""
	if 0 == maxlines:
		""" If user themselves has not given a maximum then we use the global
		default which is set large enough to deal with most / all cases
		"""
		maxlines = MAXLINES_LIST

	lines_for_list = []
	if maxwidth > 0:
		for idx,line in enumerate(lines):
			if idx > maxlines:
				break
			if maxwidth > 0:
				lines_for_list.append(line[:maxwidth])
			else:
				lines_for_list.append(line)
	elif maxlines > 0:
		for idx,line in enumerate(lines):
			if idx > maxlines:
				break
			lines_for_list.append(line)
			#print(len(lines_for_list))
	else:
		# Because of MAXLINES_LIST we are unlikely to fall through to here
		lines_for_list = lines[:]

	list_opening_closing = [opening_linenum,closing_linenum,opening_line,lines_for_list]

	return list_opening_closing


def content_search(fileoutput_handle,fileinput_given):
	""" Retained for debug / test purposes.
	See newer content_search_re() which accepts a further argument
	"""
	global total_linecount
	global retain_lines
	# Above ensures we have access to the global list 'retain_lines'
	dict_of_lists = {}
	idx = 0
	blanks_sequential = 0
	stored_line = ''
	opening_linenum = 0
	opening_line = None
	closing_linenum = 0
	target_lines = []
	for line in fileinput.input(fileinput_given):
		idx += 1
		linesent_flag = 0
		if RE_BLANK_WITH_SPACE.match(line):
			blanks_sequential+=1
			if PYVERBOSITY > 1:
				print("Have blanks_sequential now {0}".format(blanks_sequential))
			if opening_linenum > 0 and blanks_sequential > 1:
				closing_linenum = idx
				blanks_sequential = 0
			else:
				closing_linenum = 0
			if opening_linenum > 0:
				target_lines.append(line)
				linesent_flag = 1
			stored_line = line
			retain_flags.append(0)
			if blanks_sequential > 0:
				# We have not 'closed' and are still in 'count the blanks' mode
				continue
		else:
			blanks_sequential=0

		if RE_RETURN_PATH.match(line) or RE_RETURN_PATH_OLDER.match(line):
			if RE_FROM.match(stored_line):
				if PYVERBOSITY > 1:
					print("Matched 'return path' ahead of matching from shown next...")
					print("...{0}".format(stored_line))
				try:
					retain_flags[-1]=0
					#retain_flags[idx-1]=0
					# Above is less preferred than [-1] as idx is human (1 indexed) 
				except IndexError:
					print("idx-1 lookup failed using idx={0}".format(idx))
					print(retain_flags)
					raise
				# Above is setting 0 for retained flags for the stored_line 'From'
				retain_lines.pop()
				# Above is REMOVING the stored_line 'From' from the retain_lines list

				opening_line = stored_line
				target_lines = [stored_line]
				target_lines.append(line)
				linesent_flag = 1
				opening_linenum = idx-1
				closing_linenum = 0
				retain_flags.append(0)

		if opening_linenum > 0 and 0==linesent_flag:
			# We are 'open' and have an unsent line
			target_lines.append(line)

		if closing_linenum > 0:

			if PYVERBOSITY > 1:
				#print(len(target_lines))
				prefix_str = 'First 3 arguments to lists_from_target next'
				print("{0} will be {1},{2},{3}".format(prefix_str,opening_linenum,
									closing_linenum,opening_line))

			list_closed = list_from_target(opening_linenum,closing_linenum,
							opening_line,target_lines)
			if len(list_closed) > 0:
				for line in target_lines:
					fileoutput_handle.write(line)
				fileoutput_handle.flush()
			dict_of_lists[closing_linenum] = list_closed
			# Having completed processing of a closed target, we next set 0s for important flags
			opening_linenum = 0
			opening_line = ''
			closing_linenum = 0
			target_lines = []
			""" Next we will repeat the stored_line assignment that normally happens
			at loop end, then shortcut out with a 'continue'
			This ensure we do not retain the final blank in a block.
			"""
			stored_line = line
			continue

		if 0==len(target_lines):
			retain_flags.append(1)
			if PYVERBOSITY > 1:
				print("Retaining this:{0}".format(line))
			retain_lines.append(line)
		stored_line = line
	total_linecount = idx
								
	return dict_of_lists


def content_search_re(regex_compiled,fileoutput_handle,fileinput_given,verbosity=0):
	""" First argument is a compiled regex.
	Supercedes older content_search() which does not accept regex argument

	LineBlock = namedtuple('LineBlock', ['opening_linenum','opening_line','preopening_line',
                                       'closing_linenum','blanks_sequential','target_lines'])

	Approach for content_search_re is different to earlier implementations, in that we
	build up 'retain_lines' on assumption every non-blank line does not belong to a target,
	but then slice off the relevant tail each time a target is closed.
	"""
	global total_linecount
	global retain_lines
	global retain_flags
	# Above ensures we have access to the global list 'retain_lines'
	retain_lines = []
	""" Above not really necessary for a 'once through' typical run, however
	a test suite might call this function multiple times, so force to empty list
	"""
	if verbosity > 0 or PYVERBOSITY > 1:
		""" Here we ensure if given a command line verbosity > 0
		then it forces verbosity > 0 regardless of any [possibly]
		lower PYVERBOSITY
		"""
		verbosity = max(verbosity,PYVERBOSITY)
	dict_of_lists = {}
	idx = 0
	stored_line = ''
	bk = LineBlock()
	#bk.blanks_sequential = 0
	target_lines = []

	if fileoutput_handle is None:
		# This block intended for test harness use
		prefix_str = 'Test harness driven lines list fileinput_given'
		len_lines = 0
		try:
			len_lines = len(fileinput_given)
			lines_iterable = fileinput_given[:]
			if verbosity > 1:
				print("{0} len={1} during testing.".format(prefix_str,len_lines))
		except:
			if verbosity > 0:
				print("{0} len={1} has issue.".format(prefix_str,len_lines))
			lines_iterable = []
	else:
		try:
			lines_iterable = fileinput.input(fileinput_given)
		except:
			if verbosity > 0:
				print("Issue with fileinput_given={0}".format(fileinput_given))
			raise

	for line in lines_iterable:
		idx += 1
		retain_lines.append(line)
		if RE_BLANK_WITH_SPACE.match(line):
			bk.blanks_sequential_increment()
			prefix_str = "have blanks_sequential now"
			if verbosity > 2:
				print(".open()={0} ... {1} {2}".format(bk.open(),prefix_str,bk.blanks_sequential))
			if bk.open() and bk.blanks_sequential > 1:
				target_lines.append(line)
				bk.closer(linenum=idx)
				# Above have not given target_lines so relying on later call to .closer(...,[...])
				#bk.closing_linenum = idx
				if verbosity > 1:
					prefix_str = 'Have just set'
					suffix_str = 'TO MARK [provisionally] ** CLOSED **'
					print("{0} bk.closing_linenum={1} {2}".format(prefix_str,
						bk.closing_linenum,suffix_str))
				bk.blanks_sequential_zero()
			elif bk.open():
				# blanks_sequential < 2
				prefix_str = "have blanks_sequential now"
				if verbosity > 2:
					print(".open()={0} ... {1} {2}".format(bk.open(),prefix_str,bk.blanks_sequential))
				target_lines.append(line)

			else:
				# If not already open(), then call .opener() to set correct flags/state
				if verbosity > 1:
					print("Opening using opener() when line={0}".format(line))
				# opener() with no arg will just ensure closing_linenum is set 0
				bk.opener()

			stored_line = line
			retain_flags.append(0)
			if bk.blanks_sequential > 0:
				if verbosity > 1:
					prefix_str = "Have not 'closed' and are still in"
					print("{0} 'count the blanks' ({1}) mode.".format(prefix_str,
										bk.blanks_sequential))
				continue
		else:
			if verbosity > 2:
				print("calling blanks_sequential_zero() for ZEROing.")
			bk.blanks_sequential_zero()
		if verbosity > 2:
			print("Have blanks_sequential now {0} post block".format(bk.blanks_sequential))


		if RE_RETURN_PATH.match(line) or RE_RETURN_PATH_OLDER.match(line):
			if RE_FROM.match(stored_line):
				if verbosity > 1:
					print("Matched 'return path' ahead of matching from shown next...")
					print("...{0}".format(stored_line))
				bk = LineBlock()
				try:
					retain_flags[-1]=0
					#retain_flags[idx-1]=0
					# Above is less preferred than [-1] as idx is human (1 indexed) 
				except IndexError:
					print("idx-1 lookup failed using idx={0}".format(idx))
					print(retain_flags)
					raise
				# Above is setting 0 for retained flags for the stored_line 'From'
				bk.opener(linenum=(idx-1),opening=line,preopening=stored_line,lines=[stored_line,line])
				target_lines = [stored_line]
				""" Above is auto appended with 'line' by the later bk.open() block so no need to 
				preempt things by doing = [stored_line,line]
				"""
				if verbosity > 2:
					print(target_lines)
				#closing_linenum = 0
				retain_flags.append(0)

		if verbosity > 2:
			# Helpful for verbose debug to show .open() and .closed()
			print(idx,bk.open(),bk.closed())

		if bk.closed():

			""" Before clearing target_lines we need to slice off the appropriate tail from
			global list retain_lines PROVIDED target string/regex really was found
			"""
			removal_count = 0
			for idy,line in enumerate(target_lines):
				if regex_compiled.search(line):
					if verbosity > 1:
						prefix_str = 'regex_compiled MATCH at target line'
						suffix_str = '** MATCH **'
						print("{0} idy={1} ... {2}".format(prefix_str,idy,suffix_str))
					removal_count = len(target_lines)
					break

			if verbosity > 2:
				print("Comparing removal_count={0} and len(target_lines)={1} to establish if target MISS".format(
													removal_count,len(target_lines)))
			if removal_count < len(target_lines):
				# target MISS - we never had a match for the string/regex
				midfix_str = 'lines from retain_lines so expect length TO REMAIN at'
				if verbosity > 1:
					print("NOT dropping final {0} {1} {2}".format(removal_count,
										midfix_str,len(retain_lines)))
				bk.closer(linenum=bk.closing_linenum,lines=[])
				""" Note the empty list supplied as final argument above.
				That fact is used in logic after this if/else block
				"""
				
			else:
				# Something positive to do instead of just reporting - drop em please
				midfix_str = 'lines from retain_lines the length is'
				if verbosity > 1:
					print("Before dropping final {0} {1} {2}".format(removal_count,
											midfix_str,len(retain_lines)))
				bk.closer(linenum=bk.closing_linenum,lines=target_lines)
				# Having tried +1 and +2 we settled on functioning (removal_count+0) below	
				retain_lines = retain_lines[:-(removal_count+0)]

			if len(bk.target_lines) < 3:
				""" The < 3 test here could instead be replaced by a comparison
				of len(target_lines) and len(bk.target_lines) and should give
				same outcome in most cases.
				Result is the same, we want to SKIP adding thing to dict_of_lists as irrelevant
				"""
				if verbosity > 2 and '02:06:18 2010' in bk.target_lines[:-1]:
					print('lenny5mail.txt lines that should not be included are caught.')
				prefix_str = 'Skipping adding to dict_of_list as'
				if verbosity > 1:
					print("{0} len(target_lines)={1}".format(prefix_str,len(bk.target_lines)))
			elif len(bk.target_lines) > 0:
				# Have more to do as target string/regex REALLY WAS FOUND in the lines
				if verbosity > 1:
					#print(len(target_lines))
					prefix_str = 'First 3 arguments to lists_from_target next'
					print("{0} will be {1},{2},{3}".format(prefix_str,bk.opening_linenum,
										bk.closing_linenum,bk.opening_line))

				list_closed = list_from_target(bk.opening_linenum,bk.closing_linenum,
								bk.opening_line,bk.target_lines)
				if len(list_closed) > 0 and fileoutput_handle is not None:
					for line in bk.target_lines:
						fileoutput_handle.write(line)
					fileoutput_handle.flush()
				dict_of_lists[bk.closing_linenum] = list_closed
				# Final processing of successful target match is now complete

			target_lines = []
			# Having completed processing of a .closed() target, we next set lb back to defaults
			bk = LineBlock()
			if verbosity > 1:
				print("bk now holds default line block")
		elif bk.open():
			target_lines.append(line)
		else:
			if len(retain_flags) < idx:
				retain_flags.append(1)
			else:
				retain_flags[idx-1]=1

		stored_line = line

	total_linecount = idx
								
	return dict_of_lists


def mailretain_write(lines,verbosity=0):
	""" 
	Write to nonmatching.txt the lines from original file that
	did not meet our target criteria
	"""
	idx=0
	with open('/tmp/nonmatching.txt', 'w') as fret:
		for line in lines:
			idx += 1
			fret.write(line)
			#if retain_flags[idx] > 0:

	if verbosity > 1:
		print("retain_flags iterated was {0}".format(idx))

	return


if __name__ == '__main__':
	exit_rc = 0

	section_label = None

	pyregex_string = '\W'
	re_compiled = None
	if len(argv) > 1:
		pyregex_string = argv[1]
	else:
		exit_rc = 110

	try:
		re_compiled = re.compile("{0}".format(pyregex_string),re.IGNORECASE)
	except IndexError:
		exit_rc = 120
		raise
	""" Two design decisions above. We are choosing to give IGNORECASE flag
	so that regardless of case of text given by user, we do a case-insensitive match
	Secondly we are not using r'' so special characters that would only be
	accepted in a 'raw' call are not treated as raw.
	"""

	if 0 == exit_rc:

		fileinput_given = argv[2:]

		if PYVERBOSITY > 1:
			print(re_compiled)

		match_dict = {}
		"""
		with open('/tmp/mail.txt', 'w') as outfile:
			match_dict = content_search(outfile,fileinput_given)
			#match_dict = content_search()
		"""
		with open('/tmp/mail.txt', 'w') as outfile:
			match_dict = content_search_re(re_compiled,outfile,fileinput_given)

		""" We will likely use the method signature shown below next
		dict_summary_block(dict_given,summary_level=1,print_flag=False):
		# summary_level=1 sees any printed output just showing the LENGTH
		"""
		len_match_dict = len(match_dict)
		if PYVERBOSITY > 2 and len_match_dict < 2:
			if len(match_dict) == 1:
				print("Match dictionary has a SINGLE ENTRY.")
			else:
				print("Match dictionary has low hit count of {0}".format(len_match_dict))

		if PYVERBOSITY < 2:
			dict_summary(match_dict,summary_level=1,print_flag=False,prefix_string='Match')
		elif PYVERBOSITY < 3:
			sumdict = dict_summary_dict(match_dict,summary_level=1,print_flag=True,prefix_string='Match')
			#print("Match dictionary contains {0} entries.".format(len(match_dict)))
		elif PYVERBOSITY < 4:
			sumdict = dict_summary_dict(match_dict,summary_level=2,print_flag=True,prefix_string='Match')
		else:
			sumdict = dict_summary_dict(match_dict,summary_level=5,print_flag=True,prefix_string='Match')

		""" Most of next loop was concerned with retention handling, however
		now just a debug helper as retain_flags is not preferred way of 
		organising output to be sent to mailretain.txt
		"""

		stored_vlist2 = [0,0]
		stored_end = 0
		for mkey in sorted(match_dict):
			vlist = match_dict[mkey]
			vlist2 = vlist[:2]
			if PYVERBOSITY > 1:
				print(vlist[:3])
			start = vlist2[0]
			end = vlist2[1]
			if start > (stored_end+1):
				# Something in orginal file to be retained in mailretain.txt
				retain_start = stored_end+1
				#duration = (stored_end+1)-(start-1)

		if PYVERBOSITY > 1:
			print("First pass total_linecount={0}".format(total_linecount))

		if total_linecount > 2:
			
			if PYVERBOSITY > 1:
				print(sum(retain_flags))
			mailretain_write(retain_lines,verbosity=(PYVERBOSITY>1))

		#if fileinput_given[0].startswith(chr(47)):

	exit(exit_rc)

