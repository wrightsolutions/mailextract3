#!/usr/bin/env python
# -*- coding: utf-8 -*-
from unittest import TestCase
from os import linesep
import re
from string import ascii_lowercase,digits,printable,punctuation

import varmailextract3 as mex

class Printable(TestCase):

	def setUp(self):

		# chr(33) is exclamation mark (!)      ; chr(34) is double quote (")     ; chr(39) is single quote (')
		# chr(35) is hash / octothorpe             ; chr(58) is colon (:)            ; chr(61) is equals (=)
		# chr(45) is hyphen			; chr(64) is at symbol (@)
		# chr(32) is space ; chr(10) is line feed

		self.SET_COLON_SPACE_EQUALS = set([chr(58),chr(32),chr(61)])
		self.SET_PRINTABLE = set(printable)
		self.SET_PUNCTUATION = set(punctuation)
		self.SET_LOWER_PLUS_DIGITS = set(ascii_lowercase).union(digits)
		self.TARGET_WARN = '--WARN--'

		self.STUFF1 = 'Some stuff'
		self.STUFF2 = 'More stuff'
		self.STUFF3 = 'Even more stuff'

		self.HEAD2NEWLINES = "{0}{0}".format(chr(10))
		self.HEAD2NEWLINES_SPLIT = ['','']
		#
		self.TAIL2NEWLINES = "{0}{1}{0}{1}".format(chr(32),chr(10))
		self.TAIL2NEWLINES_SPLIT = [' ',' ']
		#

		self.WARN12PROC = """
--WARN-- [lin003w] The process `apt-cacher-ng' is present
--WARN-- [lin003w] The process `avahi-daemon' is present
--WARN-- [lin003w] The process `avahi-daemon' is present
--WARN-- [lin002i] The process `dhclient' is present
--WARN-- [lin002i] The process `dhclient' is present
--WARN-- [lin003w] The process `dnsmasq' is present
--WARN-- [lin003w] The process `dnsmasq' is present
--WARN-- [lin003w] The process `rpc.statd' is present
--WARN-- [lin003w] The process `rpc.statd' is present
--WARN-- [lin002i] The process `rpcbind' is present
--WARN-- [lin002i] The process `rpcbind' is present
--WARN-- [lin002i] The process `rpcbind' is present
"""
		self.WARN12PROC_SPLIT = self.WARN12PROC.split(linesep)

		self.WARNHEAD2016_TEMPLATE = """


From root@f1.example.com Sun Feb {0} 14:00:{0} 2016
Return-path: <root{0}@f1.example.com>
Envelope-to: root@f1.example.com
Delivery-date: Sun, {0} Feb 2016 14:00:{0} +0000
Received: from root{0} by f1.example.com with local (Mailer 4.84)
	(envelope-from <root{0}@f1.example.com>)
	id 1aXUYL-0004Ku-Td
	for root@f1.example.com; Sun, {0} Feb 2016 14:00:{0} +0000
From: "auditor at f1" <root{0}@f1>
To: root@f1.example.com
Subject: Report for f1
Message-Id: <E1aXUYL-0004Ku-Td@f1.example.com>
Date: Sun, {0} Feb 2016 14:00:{0} +0000


# Checking processes and report --WARN and similar
"""

		self.WARNHEAD2016ONE_SPLIT = (self.WARNHEAD2016_TEMPLATE.format(1)).split(linesep)
		self.WARNHEAD2016TWO_SPLIT = (self.WARNHEAD2016_TEMPLATE.format(2)).split(linesep)
		self.WARNHEAD2016THREE_SPLIT = (self.WARNHEAD2016_TEMPLATE.format(3)).split(linesep)


		self.WARN2016ONE = self.WARNHEAD2016ONE_SPLIT[:]
		self.WARN2016ONE.extend(self.WARN12PROC_SPLIT)
		self.WARN2016ONE.extend(self.TAIL2NEWLINES_SPLIT)

		self.WARN2016TWO = self.WARNHEAD2016TWO_SPLIT[:]
		self.WARN2016TWO.extend(self.WARN12PROC_SPLIT)
		self.WARN2016TWO.extend(self.TAIL2NEWLINES_SPLIT)

		self.WARN2016THREE = self.WARNHEAD2016THREE_SPLIT[:]
		self.WARN2016THREE.extend(self.WARN12PROC_SPLIT)
		self.WARN2016THREE.extend(self.TAIL2NEWLINES_SPLIT)

		self.WARN2016DOUBLE_SPLIT = self.WARN2016ONE[:]
		self.WARN2016DOUBLE_SPLIT.extend(self.WARN2016TWO[:])
		self.WARN2016DOUBLE = '\n'.join(self.WARN2016DOUBLE_SPLIT)
		self.WARN2016ONE_SPLIT = self.WARN2016ONE[:]
		self.WARN2016ONE = '\n'.join(self.WARN2016ONE_SPLIT)
		# Next we form triple by just adding the head and tail to our already constructed double
		self.WARN2016TRIPLE_SPLIT = self.WARN2016DOUBLE_SPLIT[:]
		self.WARN2016TRIPLE_SPLIT.extend(self.WARNHEAD2016THREE_SPLIT[:])
		self.WARN2016TRIPLE_SPLIT.extend(self.TAIL2NEWLINES_SPLIT)
		self.WARN2016TRIPLE = '\n'.join(self.WARN2016TRIPLE_SPLIT)

		# Regular expressions are defined next and generally are named RE_...
		self.RE_PREF2DASH_WARN = re.compile("{0}{0}warn{0}{0}".format(chr(45)),re.IGNORECASE)
		self.RE_PREF2DASH_WARN_LOOSE = re.compile("{0}{0}\s*warn".format(chr(45)),re.IGNORECASE)
		self.RE_ROOT2AT = re.compile("root{0}{1}".format(2,chr(64)))
		self.RE_RPCBIND = re.compile('rpcbind')

		# Next we have a commented out section useful if you want a copy of test data to /tmp/
		"""
		with open('/tmp/testdata.txt','w') as f:
			for line in self.WARN2016TRIPLE_SPLIT:
				f.write("{0}\n".format(line))
			f.flush()
		# Copy of above self.WARN2016TRIPLE_SPLIT is saved in input/lenny5triple.txt
		"""
		return


	def test_warn1(self):
		""" Test of finding RE_PREF2DASH_WARN in WARN2016ONE lines
		"""
		self.assertEqual(len(self.WARN2016ONE_SPLIT),36)
		search_verbosity=0
		match_dict = mex.content_search_re(self.RE_PREF2DASH_WARN,None,
				self.WARN2016ONE_SPLIT,search_verbosity)
		#print(match_dict)
		SUMMARY_LEVEL=2
		sumdict = mex.dict_summary_dict(match_dict,summary_level=SUMMARY_LEVEL,
					print_flag=False,prefix_string='test_warn Match')
		self.assertEqual(len(sumdict),1)
		return


	def test_warn2matches(self):
		""" Test of finding RE_PREF2DASH_WARN in WARN2016TRIPLE lines
		"""
		self.assertEqual(len(self.WARN2016TRIPLE_SPLIT),94)
		search_verbosity=0
		match_dict = mex.content_search_re(self.RE_PREF2DASH_WARN,None,
				self.WARN2016TRIPLE_SPLIT,search_verbosity)
		#print(match_dict)
		SUMMARY_LEVEL=2
		sumdict = mex.dict_summary_dict(match_dict,summary_level=SUMMARY_LEVEL,
					print_flag=False,prefix_string='test_warn2matches Match')
		self.assertEqual(len(sumdict),2)
		return


	def test_root2(self):
		""" Test of finding RE_ROOT2AT in WARN2016TRIPLE lines
		"""
		self.assertEqual(len(self.WARN2016TRIPLE_SPLIT),94)
		search_verbosity=0
		match_dict = mex.content_search_re(self.RE_ROOT2AT,None,
				self.WARN2016TRIPLE_SPLIT,search_verbosity)
		#print(match_dict)
		SUMMARY_LEVEL=5
		sumdict = mex.dict_summary_dict(match_dict,summary_level=SUMMARY_LEVEL,
					print_flag=True,prefix_string='test_root2 Match')
		self.assertEqual(len(sumdict),1)
		#print(mex.retain_lines)
		return


	def test_warn_loose(self):
		""" Test of finding RE_PREF2DASH_WARN_LOOSE 
		For debugging the following signature may prove useful:
		mex.dict_summary_dict(dict_given,summary_level=1,print_flag=False,prefix_string='')
		"""
		return
		#print('zzzloose')
		#print(self.WARN2016TRIPLE_SPLIT)
		#
		self.assertEqual(len(self.WARN2016TRIPLE_SPLIT),94)
		#
		search_verbosity=2
		match_dict = mex.content_search_re(self.RE_PREF2DASH_WARN_LOOSE,None,
				self.WARN2016TRIPLE_SPLIT,search_verbosity)
		print(match_dict)
		SUMMARY_LEVEL=2
		sumdict = mex.dict_summary_dict(match_dict,summary_level=SUMMARY_LEVEL,
					print_flag=True,prefix_string='Loose Match')
		self.assertEqual(len(sumdict),3)
		"""
                if PYVERBOSITY > 2 and len(match_dict) == 1:
                        print("Match dictionary has a SINGLE ENTRY.")

                if PYVERBOSITY < 1:
                        dict_summary(match_dict,summary_level=1,print_flag=False,prefix_string='Match')
                elif PYVERBOSITY < 2:
                        sumdict = dict_summary_dict(match_dict,summary_level=1,print_flag=True,prefix_string='Match')
                        #print("Match dictionary contains {0} entries.".format(len(match_dict)))
                elif PYVERBOSITY < 3:
                        sumdict = dict_summary_dict(match_dict,summary_level=2,print_flag=True,prefix_string='Match')
                else:
                        sumdict = dict_summary_dict(match_dict,summary_level=5,print_flag=True,prefix_string='Match')
		"""
		return


	def test_rpcbind(self):
		""" Test of finding RE_RPCBIND in WARN2016TRIPLE lines
		"""
		self.assertEqual(len(self.WARN2016TRIPLE_SPLIT),94)
		search_verbosity=0
		match_dict = mex.content_search_re(self.RE_RPCBIND,None,
				self.WARN2016TRIPLE_SPLIT,search_verbosity)
		#print(match_dict)
		SUMMARY_LEVEL=5
		sumdict = mex.dict_summary_dict(match_dict,summary_level=SUMMARY_LEVEL,
					print_flag=True,prefix_string='test_root2 Match')
		self.assertEqual(len(sumdict),2)
		self.assertEqual(len(mex.retain_lines),28)
		#print(mex.retain_lines)
		return



