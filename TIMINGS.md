# Timing of fully OO version versus namedtuple version

* real	0m33.927s
* user	0m32.260s
* sys	0m5.640s

Above suggests a 20 to 25% performance improvement in the class based newer implementation

( Probably this is due to the saving from try OO and not having to have nt copies  
after each update to an nt field )


## Overriding namedtuple - halfway to OO

Timings for 641MB input with search pattern 'logcheck'

* real	0m43.374s
* user	0m41.132s
* sys	0m3.644s


```
printf "cat mail.txt | sed 's/^From / \\\n \\\nFrom /' | python ./varmailextract.py 'logcheck'" > /tmp/varmailextract_logcheck.sh
chmod 750 /tmp/varmailextract_logcheck.sh
time /tmp/varmailextract_logcheck.sh
```

Note: Do replace /tmp/ above with a more secure path if you prefer not to operate .sh out of /tmp/


## Choice of search pattern and effect on timings

The design choices we have made have been with future adaptability / specialisation in mind

This means the code is certainly less efficient than an optimised 'one purpose' variant

The longer the search pattern, the less temporary memory copying has to take place.

And so try and choose search patterns as long as possible for best results

Even with a 641MB input, if I choose a search pattern of only 4 characters I can  
extend the runtime to 4 minutes on comparable machine to timings above.

Patterns of 8 or longer are much better for your timings.


## Python 3 versus Python 2 of the namedtuple implementation - no significant difference in timings

Python 3 run against the same 641MB input file searching for 'logcheck' produced almost identical timings

* real	0m42.948s
* user	0m44.144s
* sys	0m5.056s


